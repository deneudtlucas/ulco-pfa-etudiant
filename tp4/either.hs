type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
mySqrt x = if x < 0
    then Left "sqrt negatif"
    else Right (sqrt x)

myLog :: Double -> MyNum
myLog x = if x < 0
    then Left "log negatif"
    else Right (log x)

myMul2 :: Double -> MyNum
myMul2 x = Right (n * 2)

myNeg :: Double -> MyNum
myNeg x = Right (-x)

myCompute :: MyNum
myCompute = 
    case mySqrt 16 of
        Left err -> Left err
        Right r1 -> case myNeg r1 of
            Left err -> Left err
            Right r2 -> case myMul2 r1 of
                Left err -> Left err
                Right r3 -> Right r3

myCompute2 :: MyNum
myCompute2 = do
    r1 <- mySqrt (-16)
    r2 <- myNeg r1
    myMul2 r2

main :: IO ()
main = do
    putStrLn $ showMyNum myCompute
    print myCompute

    print $ do
        r1 <- mySqrt 16
        r2 <- myNeg r1
        myMul2 r2

