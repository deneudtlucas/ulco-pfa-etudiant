import Data.Char (toUpper)
import Data.List.Split (splitOn)

data User = User
    { _name :: String
    , _email :: String
    } deriving Show

parseUser :: String -> Maybe User
parseUser str =
    ??? splitOn ";" str ???

-- upperize :: User -> User

main :: IO ()
main = do
    print $ parseUser "toto;toto@tata.com"
    print $ parseUser "nimp"

