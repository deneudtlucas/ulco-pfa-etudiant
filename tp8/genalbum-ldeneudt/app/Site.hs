{-# LANGUAGE DeriveGeneric #-}
module Site where

import Data.Aeson
import GHC.Generics
    
data Site = Site
    { imgs :: [String]
    , url  :: String
    } deriving (Generic, Show)
    
instance FromJSON Site

loadDatas :: String -> IO (Either String [Site])
loadDatas = eitherDecodeFileStrict
