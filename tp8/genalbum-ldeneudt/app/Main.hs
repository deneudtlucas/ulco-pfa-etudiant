import Album
import Site

main :: IO ()
main = do
    datasE <- loadDatas "app/data/genalbum.json"
    case datasE of
        Left err -> putStrLn err
        Right datas -> 
            genAlbum "out.html" datas