{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import           Data.Aeson
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address :: Address
    } deriving (Generic, Show)

data Address = Address
    { road :: T.Text
    , zipcode :: Int
    , city :: T.Text
    , number :: Int
    } deriving (Generic, Show)

instance ToJSON Person where

    toJSON (Person firstname lastname birthyear address)=
        object ["first" .= firstname, "birth" .= birthyear, "last" .= lastname, "address" .= address ]
instance ToJSON Address where
    toJSON (Address road zipcode city number)=
        object ["road" .= road, "zipcode" .= zipcode, "city" .= city, "number" .= number ]
        
persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 
        (Address "Pont Vieux" 42 "Espaly" 43000 )
    , Person "Haskell" "Curry" 1900
        (Address "Pere Lachaise" 1337 "Paris" 75000 )
    ]

main :: IO()
main = do
    encodeFile "out-aeson3.json" persons
