{-# LANGUAGE OverloadedStrings #-}
import Lucid

myHtml :: Html ()
myHtml = (h1_ "hello" <> p_ "word")

main :: IO()
main = do
    print myHtml