import Database.SQLite.Simple (open, close)

import Movie1
import Movie2

main :: IO ()
main = do
    conn <- open "movie.db"

    putStrLn "\nMovie1.dbSelectAllMovies"
    allMovies <- Movie1.dbSelectAllMovies conn
    print allMovies

    putStrLn "\nMovie1.dbSelectAllMovies"
    allProds <- Movie1.dbSelectAllProds conn
    print allProds

    close conn