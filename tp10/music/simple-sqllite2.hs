{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Artist = Artist 
    { _artist_id    :: Int
    , _artist_name :: Text
    } deriving (Generic, Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field 

selectAllArtiste :: Connection -> IO [Artist]
selectAllArtiste conn = query_ conn 
    "SELECT artist_id, artist_name FROM artist"


main :: IO()
main = withConnection "music.db" selectAllArtiste >>= mapM_ print