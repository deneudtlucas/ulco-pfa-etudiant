CREATE TABLE country (
    country_id INTEGER PRIMARY KEY AUTOINCREMENT,
    country_name TEXT
);

CREATE TABLE city (
    city_id INTEGER PRIMARY KEY AUTOINCREMENT,
    city_country INTEGER,
    city_name TEXT,
    FOREIGN KEY(city_country) REFERENCES country(country_id)
);

INSERT INTO country VALUES(1, 'BRETAGNE');
INSERT INTO country VALUES(2, 'france');
INSERT INTO country VALUES(3, 'Uruguay');
INSERT INTO city VALUES(1, 3, 'Montevideo');
INSERT INTO city VALUES(2, 1, 'Rennes');
INSERT INTO city VALUES(3, 2, 'Blavozy');
