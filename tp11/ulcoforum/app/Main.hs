{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Database.SQLite.Simple
import Web.Scotty
import Lucid
import Data.Text
import TextShow
import qualified Data.Text as T
import GHC.Generics (Generic)
import Control.Monad.IO.Class(liftIO)

data Message = Message
    { _mid :: Int,
      _discussionid :: Int,
      _mname :: Text,
      _message :: Text
    } deriving (Generic, Show)

instance FromRow Message where
    fromRow = Message <$> field <*> field <*> field <*> field

data Discussion = Discussion
    { _did :: Int,
      _dname :: Text
    } deriving (Generic, Show)

instance FromRow Discussion where
    fromRow = Discussion <$> field <*> field

main :: IO ()
main = scotty 3000 $ do
    get "/" $ html $ renderText (getIndex menu)
    get "/alldata" $ html $ renderText (alldata menu)
    get "/allthreads" $ do
        threads <- liftIO $ withConnection "ulcoforum.db" dbgetallthreads
        html $ renderText (allthreads menu threads)
    get "/onethread/:id" $ do
        id <- param "id"
        threads <- liftIO $ withConnection "ulcoforum.db" dbgetallthreads
        messages <- liftIO $ withConnection "ulcoforum.db" dbgetallonethread
        let parseddiscussion = Prelude.filter (\(Discussion did _) -> did == id) threads
        let parsedmessages = Prelude.filter (\(Message _ did _ _) -> did == id) messages
        html $ renderText (oneThread menu parsedmessages (Prelude.head parseddiscussion))

menu :: Html ()
menu = do
    doctype_
    html_ $ do
        head_ $ meta_ [charset_ "utf8"]
        body_ $ do
            a_ [href_ "/"] $ do
                h1_ "ulcoforum"
            a_ [href_ "/alldata"] "alldata"
            span_ $ toHtml $ T.pack " - "
            a_ [href_ "/allthreads"] "allthreads"

getIndex :: (Html ()) -> Html ()
getIndex menu = html_ $ do
    menu
    p_ $ toHtml $ T.pack "This is ulcoforum"

alldata :: (Html ()) -> Html ()
alldata menu = html_ $ do
    menu
    p_ $ toHtml $ T.pack "All data:"

allthreads :: (Html ()) -> [Discussion] -> Html ()
allthreads menu threads = html_ $ do
    menu
    p_ $ toHtml $ T.pack "All threads:"
    mapM_ viewthreadslink threads

viewthreadslink :: Discussion -> Html()
viewthreadslink (Discussion id name) = do
    li_ $ a_ [href_ ("/onethread/" <> showt id)] $ toHtml $ T.pack (show name)

dbgetallthreads :: Connection -> IO [Discussion]
dbgetallthreads conn = query_ conn "SELECT * FROM discussion"

oneThread :: (Html ()) -> [Message] -> Discussion -> Html ()
oneThread menu messages (Discussion _ title) = html_ $ do
    menu
    h1_ $ toHtml title
    mapM_ viewmessage messages

dbgetallonethread ::  Connection -> IO [Message]
dbgetallonethread conn = do
    query_ conn "SELECT * FROM message"

viewmessage :: Message -> Html()
viewmessage (Message id did name message) = do
    li_ $ do
        toHtml name
        ": "
        toHtml $ message