-- create database:
-- sqlite3 ulcoforum.db < ulcoforum.sql

CREATE TABLE discussion (
    _did INTEGER PRIMARY KEY AUTOINCREMENT,
    _dname TEXT UNIQUE
);

INSERT INTO discussion (_dname) VALUES("Forum 1");
INSERT INTO discussion (_dname) VALUES("Forum 2");

CREATE TABLE message (
    _mid INTEGER PRIMARY KEY AUTOINCREMENT,
    _discussionid INTEGER,
    _mname TEXT,
    _message TEXT,
    FOREIGN KEY (_discussionid) REFERENCES discussion(_did)
);

INSERT INTO message (_discussionid, _mname, _message) VALUES(1, "Lucas", "C'est trop cool");
INSERT INTO message (_discussionid, _mname, _message) VALUES(1, "Léo", "Grave");

INSERT INTO message (_discussionid, _mname, _message) VALUES(2, "Lucas", "Bof ce forum là");
INSERT INTO message (_discussionid, _mname, _message) VALUES(2, "Léo", "Grave");