{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text.Lazy as L
import GHC.Generics
import Data.Aeson (ToJSON)

data Rider = Rider
    { name :: String
    , images :: [String]
    } deriving (Generic, Show)

instance ToJSON Rider