{-# LANGUAGE OverloadedStrings #-}

module View where

import Model

import qualified Data.ByteString.Lazy.Char8 as Char8
import qualified Data.Text as T
import Data.Aeson
import Lucid
import Lucid.Bootstrap

generateHtml :: [Rider] -> Html ()
generateHtml riders = do
    doctype_
    html_ $ do
        head_ $ meta_ [charset_ "utf8"]
        body_ [style_ "background-color: #FDF7BE"] $ do
            container_ $ do
                h1_ "ptivelo"
                mapM_ mkRider riders

mkRider :: Rider -> Html ()
mkRider rider = row_ $ do
    h2_ $ toHtml $ name rider
    mapM_ mkImg (images rider)

mkImg :: String -> Html ()
mkImg image = div_ [style_ "display:inline-block; text-align:center"] $ do
    a_ [href_ (T.pack image)] $ do
        img_ [src_ (T.pack image), style_ "width:200px; margin:15px"]
    p_ $ toHtml $ T.pack image