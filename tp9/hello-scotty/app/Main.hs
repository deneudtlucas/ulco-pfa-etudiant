{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text.Lazy as L
import GHC.Generics
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import Web.Scotty
import Data.Aeson (ToJSON)
import Lucid

indexPage :: Html ()
indexPage = do
    doctype_
    body_ $ do
        h1_ "hello-scotty"
        ul_ $ do
            li_ $ a_ [href_ "/route1"] "/route1"
            li_ $ a_ [href_ "/route1/route2"] "/route2"
            li_ $ a_ [href_ "/html1"] "/html1"
            li_ $ a_ [href_ "/json1"] "/json1"
            li_ $ a_ [href_ "/json2"] "/json2"
            li_ $ a_ [href_ "/add1/20/22"] "/add/20/22"
            li_ $ a_ [href_ "/add2?x=20&y=22"] "/add2?x=20&y=22"
            li_ $ a_ [href_ "/add2?x=20"] "/add2?x=20"
            img_ [src_ "/bob.png"]
    

main :: IO ()
main = scotty 3000 $ do
    middleware $ staticPolicy $ addBase "static"
    get "/" $ html $ renderText indexPage
    get "/route1" $ text "route1"
    get "/route1/route2" $ text "route1/route2"
    get "/html1" $ html "<h1>html1</h1>"
    get "/json1" $ json (42::Int)
    get "/json2" $ json (Person "toto" 42)
    get "/add1/:x/:y" $ do
        x <- param "x"
        let xnum = read x :: Int
        y <- param "y"
        let ynum = read y :: Int
        text $ L.pack $ show (xnum+ynum)
    get "/add2" $ do
        x <- read <$> param "x" `rescue` (\_ -> return "0")
        y <- read <$> param "y" `rescue` (\_ -> return "0")
        json (x+y :: Int)
    get "/index" $ redirect "/"

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving (Generic, Show)

instance ToJSON Person