data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables [] = 0
compterOuvrables (x:xs) =
    if estWeekend x
    then compterOuvrables xs
    else compterOuvrables xs + 1


main :: IO ()
main = do
    print $ estWeekend Lundi
    print $ estWeekend Samedi
    print $ compterOuvrables [Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche]
    putStrLn "TODO"


