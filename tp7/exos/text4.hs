import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as Te
import qualified Data.Text.IO as T

main :: IO ()
main = do 
    -- lire text.hs en ByteString
    file <- T.readFile "text4.hs"

    -- convertir en String
    let str = Te.encodeUtf8 file

    -- afficher
    BS.putStrLn(str)