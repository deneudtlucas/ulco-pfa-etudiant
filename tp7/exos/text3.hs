import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as Te
import qualified Data.Text.IO as T

main :: IO ()
main = do 
    -- lire text.hs en ByteString
    file <- BS.readFile "text3.hs"

    -- convertir en String
    let str = Te.decodeUtf8 file

    -- afficher
    T.putStrLn(str)