import qualified Data.ByteString.Char8 as BS

main :: IO ()
main = do 
    -- lire text.hs en ByteString
    file <- BS.readFile "text1.hs"

    -- convertir en String
    let str = BS.unpack file

    -- afficher
    putStrLn(str)