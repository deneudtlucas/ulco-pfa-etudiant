{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { first    :: T.Text
    , lasts     :: T.Text
    , birth    :: Int
} deriving (Show)

instance FromJSON Person where
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> (read <$> v .: "birthyear")

main :: IO ()
main = do
    person <- eitherDecodeFileStrict "aeson-test1.json"
    print(person :: Either String Person)

    persons <- eitherDecodeFileStrict "aeson-test2.json"
    print(persons :: Either String [Person])

    persons2 <- eitherDecodeFileStrict "aeson-test3.json"
    print(persons2 :: Either String [Person])

