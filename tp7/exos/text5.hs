import qualified Data.Text.IO as Ts
import qualified Data.Text.Lazy as Tl
import qualified Data.Text.Lazy.IO as Tl

main :: IO ()
main = do 
    -- lire text.hs en ByteString
    file <- Ts.readFile "text5.hs"

    -- convertir en String
    let str = Tl.fromStrict file
    
    -- afficher
    Tl.putStrLn(str)