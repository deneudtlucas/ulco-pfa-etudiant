import qualified Data.Text.IO as T
import qualified Data.Text as T

main :: IO ()
main = do 
    -- lire text.hs en ByteString
    file <- T.readFile "text2.hs"

    -- convertir en String
    let str = T.unpack file

    -- afficher
    putStrLn(str)