{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import Data.Aeson
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person

main :: IO ()
main = do
    person <- eitherDecodeFileStrict "aeson-test1.json"
    print(person :: Either String Person)

    persons <- eitherDecodeFileStrict "aeson-test2.json"
    print(persons :: Either String [Person])

    persons2 <- eitherDecodeFileStrict "aeson-test3.json"
    print(persons2 :: Either String [Person])

